import csv
import matplotlib.pyplot as plt

def read():
    d = {}
    with open('deliveries.csv' , 'r') as matches:
        matches_csv = csv.DictReader(matches)

        for row in matches_csv:
            if row['batting_team'] not in d:
                d[row['batting_team']] = int(row['total_runs'])
            else:
                d[row['batting_team']] = int(row['total_runs']) + int(d[row['batting_team']])
    teams = list(d.keys())
    runs = list(d.values())

    return teams,runs




def display(teams , runs):
    plt.bar(teams , runs)
    plt.title('IPL total runs by each team')
    plt.xlabel('Teams')
    plt.ylabel('Total Runs')
    plt.show()

def main():
   teams, runs = read()
   display(teams, runs)




if __name__ == '__main__':
    main()