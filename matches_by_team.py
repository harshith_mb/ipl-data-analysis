import csv
import matplotlib.pyplot as plt

data = {}
s_lst = []

def read():
    with open('matches.csv', 'r') as matches:
        matches_csv = csv.DictReader(matches)

        for row in matches_csv:
            if row['season'] not in s_lst:
                s_lst.append(row['season'])

        matches.seek(0)
        next(matches)
        
        for row in matches_csv:

            if row['team1'] not in data:
                data[row['team1']] = {}
                for i in s_lst:
                     data[row['team1']].update({i:0})

            elif row['team2'] not in data:
                data[row['team2']] = {}
                for i in s_lst:
                     data[row['team2']].update({i:0})

        matches.seek(0)
        next(matches)

        for row in matches_csv:
             data[row['team1']][row['season']] += 1
             data[row['team2']][row['season']] += 1
    return data


def transform(d):
    teams = list(d.keys())
    seasons = set(d[teams[0]].keys())  # Extracting keys from the first category
    number_of_matches = {k: [v.get(k, 0) for v in d.values()] for k in seasons}

    return teams, number_of_matches

def plot_chart(teams, number_of_matches):
    plt.figure(figsize=(20, 15))
    bottom = None
    for key, val in number_of_matches.items():
        if bottom is None:
            plt.bar(teams, val, label=key)
            bottom = val
        else:
            plt.bar(teams, val, bottom=bottom, label=key)
            bottom = [bottom[i] + val[i] for i in range(len(val))]
    
    plt.xlabel('Teams')
    plt.ylabel('Number of Matches Played')
    plt.title('Season wise data of number of matches played by the teams')
    plt.xticks(rotation = 45, ha = 'right')
    plt.tight_layout()
    plt.legend(loc='upper left')
    plt.show()






def main():
    dict_data = read()
    teams, number_of_matches = transform(dict_data)
    plot_chart(teams, number_of_matches)

if __name__ == '__main__':
    main()