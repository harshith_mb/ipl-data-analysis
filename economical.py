import csv
import matplotlib.pyplot as plt

data = {}
lst = []
def read():
    with open('matches.csv','r') as matches:
        matches_csv = csv.DictReader(matches)

        for row in matches_csv:
            if row['season'] == '2015':
                lst.append(row['id'])

    with open('deliveries.csv','r') as deliveries:
        deliveries_csv = csv.DictReader(deliveries)

        for row in deliveries_csv:
            if row['match_id'] in lst:
                if row['bowler'] not in data:
                    data[row['bowler']] = [int(row['total_runs']),1]

                else:
                    data[row['bowler']][0] += int(row['total_runs'])
                    data[row['bowler']][1] += 1

    mod_dict = {k: (v[0]/v[1])*6 for k, v in data.items()}

    return mod_dict


def transform(d):
    sorted_items = sorted(d.items(), key=lambda item: item[1])
    top_list = [(k,v) for k, v in sorted_items]


    top_ten_bowler = []
    top_ten_eco = []
    count = 0
    for i,j in top_list:
        if count>10:
            break
            
        top_ten_bowler.append(i)
        top_ten_eco.append(j)
        count+=1

    return top_ten_bowler, top_ten_eco

def display_graph(top_ten_bowler , top_ten_eco):        
    plt.bar(top_ten_bowler , top_ten_eco)
    plt.title('Top 10 Economical Bowler in 2015')
    plt.xlabel('Players')
    plt.ylabel('Economy rate')
    plt.show()

def main():
    dict_data = read()
    top_ten_bowler, top_ten_eco = transform(dict_data)
    display_graph(top_ten_bowler, top_ten_eco)

if __name__ == '__main__':
    main()