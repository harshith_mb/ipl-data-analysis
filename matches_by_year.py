import csv
import matplotlib.pyplot as plt

def read():
    data = {}
    with open('matches.csv' , 'r') as matches:
        matches_csv = csv.DictReader(matches)

        for row in matches_csv:
            if row['season'] not in data:
                data[row['season']] = 1

            else:
                data[row['season']] += 1

    return(data)

def transform(d):
    years = list(d.keys())
    num_of_matches = list(d.values())
    return years, num_of_matches
    

def display_graph(years, num_of_matches):
    plt.bar(years , num_of_matches)
    plt.title('Year-Wise Number of Matches')
    plt.xlabel('Years')
    plt.ylabel('Number of matches played')
    plt.show()


def main():
    dict_data = read()
    years, num_of_matches = transform(dict_data)
    display_graph(years, num_of_matches)

if __name__ == '__main__':
    main()