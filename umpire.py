import csv
import matplotlib.pyplot as plt

def read():
    d = {}
    with open('umpires.csv' , 'r') as umpires_data:
        umpires_csv = csv.DictReader(umpires_data)

        for row in umpires_csv:
            if row['country'] == ' India':
                continue

            if row['country'] not in d:
                d[row['country']] = 1
            else:
                d[row['country']] += 1

    country = list(d.keys())
    umpire_count = list(d.values())

    return country, umpire_count


def display(country, umpire_count):
    plt.bar(country, umpire_count)
    plt.title('Umpire Data')
    plt.xlabel('Country')
    plt.ylabel('Number of umpites')
    plt.show()



def main():
    country, umpire_count = read()
    display(country, umpire_count)




if __name__ == '__main__':
    main()