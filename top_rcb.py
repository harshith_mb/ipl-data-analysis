import csv
import matplotlib.pyplot as plt

def read():
    d = {}

    with open('deliveries.csv', 'r') as batsman:
        batsman_csv = csv.DictReader(batsman)

        for row in batsman_csv:
            if(row['batting_team'] == "Royal Challengers Bangalore"):
                if row['batsman'] not in d:
                    d[row['batsman']] = int(row['batsman_runs'])
                else:
                    d[row['batsman']] += int(row['batsman_runs'])

    lst = d.items()


    sorted_items = sorted(d.items(), key=lambda item: item[1], reverse=True)
    top_list = [(k,v) for k, v in sorted_items]
    print(top_list)

    top_ten_batsman = []
    top_ten_runs = []
    count = 0

    for i,j in top_list:
        if count>10:
            break
        top_ten_batsman.append(i)
        top_ten_runs.append(j)
        count+=1

    return top_ten_batsman, top_ten_runs


def display(top_ten_batsman, top_ten_runs):
    plt.bar(top_ten_batsman , top_ten_runs)
    plt.title('Top 10 RCB Batsmen')
    plt.xlabel('Players')
    plt.ylabel('Runs Scored')
    plt.show()


def main():
    top_ten_batsman, top_ten_runs = read()
    display(top_ten_batsman, top_ten_runs)




if __name__ == '__main__':
    main()
