import csv
import matplotlib.pyplot as plt

def read():
    data={}
    lst=[]
    with open('matches.csv','r') as matches:
        matches_csv = csv.DictReader(matches)

        for row in matches_csv:
            if row['season'] == '2016':
                lst.append(row['id'])

    with open('deliveries.csv','r') as deliveries:
        deliveries_csv = csv.DictReader(deliveries)

        for row in deliveries_csv:
            if row['match_id'] in lst:
                if row['bowling_team'] not in data:
                    data[row['bowling_team']] = int(row['extra_runs'])

                data[row['bowling_team']] += int(row['extra_runs'])

    return data

def transform(d):
        bowl_teams = list(d.keys())
        extra_runs = list(d.values())

        return bowl_teams, extra_runs


def display_graph(teams , runs):
    plt.bar(teams , runs)
    plt.title('Extra Runs Conceded by Team in the Year 2016')
    plt.xlabel('Teams')
    plt.ylabel('Extra Runs Conceded')
    plt.xticks(rotation = 45, ha = 'right')
    plt.show()

def main():
   dict_data = read()
   bowl_team, extra_runs = transform(dict_data)
   display_graph(bowl_team, extra_runs)




if __name__ == '__main__':
    main()